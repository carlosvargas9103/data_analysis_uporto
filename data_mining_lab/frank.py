#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.tree import export_graphviz
import pydot
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.transforms as mtransforms
import seaborn as sns
import random


def main():

	# read in training and testing sets
	train_df = pd.read_csv('training.txt', delimiter=',',low_memory=False)
	test_df = pd.read_csv('test.txt', delimiter=',',low_memory=False)

	# Get training set labels
	train_labels = np.array(train_df['y'])
	train_labels = np.where(train_labels == "yes", 1, 0)

	# get features
	feature_columns = ['x1', 'x2', 'x3', 'x52', 'x53', 'x54', 'x55']
	train_features = train_df[feature_columns].replace('?',np.NaN).apply(pd.to_numeric)
	test_features = test_df[feature_columns].replace('?',np.NaN).apply(pd.to_numeric)

	# fill missing values with the column mean
	train_features = train_features.fillna(train_features.mean())
	test_features = test_features.fillna(test_features.mean())

	print('Training Features Shape:', train_features.shape)
	print('Training Labels Shape:', train_labels.shape)
	print('Testing Features Shape:', test_features.shape)


	## RANDOM FORREST

	# Instantiate model 
	rf = RandomForestClassifier(n_estimators= 1000, random_state=42)

	# Train the model on training data
	rf.fit(train_features, train_labels)

	# Use the forest's predict method on the test data
	test_predictions = rf.predict(test_features)

	# Use the forest's predict method on the training data
	train_predictions = rf.predict(train_features)

	# print predictions and output to file
	print(test_predictions)
	np.savetxt("test_predictions_3.csv", test_predictions.astype(int), fmt='%i', delimiter=",")

	# Model Accuracy based on training data predictions, how often is the classifier correct?
	print("Accuracy of the training dataset itself:",metrics.accuracy_score(train_labels, train_predictions))


	## CALCULATE FEATURE IMPORTANCE

	feature_importance(rf, feature_columns)


	## Visualizing a Single Decision Tree

	# Pull out one tree from the forest
	n = random.randint(1, 1000)
	tree = rf.estimators_[n]

	# Export the image to a dot file
	export_graphviz(tree, out_file = 'tree.dot', feature_names = feature_columns, rounded = True, precision = 1)

	
	print('The depth of this tree is:', tree.tree_.max_depth)
	print('The tree n is the:', n)

	# Use dot file to create a graph
	(graph, ) = pydot.graph_from_dot_file('tree.dot')

	# Write graph to a png file
	graph.write_png('tree.png'); 



def feature_importance(rf, feature_list):

	feature_imp = pd.Series(rf.feature_importances_, index=feature_list).sort_values(ascending=False)

	#%matplotlib inline
	# Creating a bar plot
	sns.barplot(x=feature_imp, y=feature_imp.index)
	# Add labels to your graph
	plt.xlabel('Feature Importance Score')
	plt.ylabel('Features')
	plt.title("Visualizing Important Features")
	plt.legend()
	plt.show()
	plt.savefig("feature_importance_plot.png")



if __name__ == '__main__':
    main()

