train <- read.csv("training.txt", sep=",", na.strings="?")

pie(table(train$y))

#Removing column ID
train <- train[,-1]

par(mfrow=c(1,2))
x <- 2:52

for(val in x) {
  cat("Variable:", val, "\n")
  a <- subset(train[,val], train$y=="no")
  b <- subset(train[,val], train$y=="yes")
  hist(a, main="NO"); 
  hist(b, main="Yes")
}



par(mfrow=c(1,1))
x <- 2:52
for(val in x) {
  cat("Variable:", val, "\n")
#the next line creates a boxplot for a row with index val
  boxplot(train[,val] ~ train$y)
}

#Decision Tree
library(rpart)
arv <- rpart(y~., train)
preds <- predict(arv, train, type="class")
table(preds, train$y)
str(preds)
testData <- read.csv("test.txt", sep=",", na.strings="?")
#Removing column ID
testData <- testData[,-1]

preds <- predict(arv, testData, type="class")
# todo : figure out the rest

str(train)
str?
help
?str
summary(train)

status(train)
df_status(train)
install.packages('funModeling')
library(funModeling)
df_status(train)
data_prof(train)
profiling_num(train)

train <- train[, -c(4:48)]
print(status(train))
plot_num(train)
#plt num makes colorful histograms and drops out Missing Values
status(train)
df_status(train)

df_status(train)
data_prof(train)
profiling_num(train)

par(mfrow=c(1,1))
x <- 1:7
for(val in x) {
  cat("Variable:", val, "\n")
  #the next line creates a boxplot for a row with index val
  boxplot(train[,val] ~ train$y)
}


par(mfrow=c(1,2))
x <- 1:7

for(val in x) {
  cat("Variable:", val, "\n")
  a <- subset(train[,val], train$y=="no")
  b <- subset(train[,val], train$y=="yes")
  hist(a, main="NO"); 
  hist(b, main="Yes")
}



par(mfrow=c(1,1))
x <- 1:7
for(val in x) {
  cat("Variable:", val, "\n")
  #the next line creates a boxplot for a row with index val
  boxplot(train[,val] ~ train$y)
}


# correlations between attributes
corr <- round(cor(train[,-8], train[,-8], use="complete"), 2)
# pearson correlation by default
install.packages('corrplot')
library('corrplot')


corrplot(corr)
corrplot(corr, tl.col = "black", order = "hclust", hclust.method = "average", addrect = 4, tl.cex = 0.7)

x1<- train[,1]
x2<- train[,2]
plot(x1,x2)
ggplot(x1,x2)


colSums(is.na(train_all))
train1 <- read.csv("training.txt", sep=",", na.strings="?")
train_all <- train1[,-1]
